import os
import imageio
import glob
import argparse
import cv2
from imageio import imread, imsave
import numpy as np

#############################################################################
# TODO: Add additional imports
#############################################################################
import matplotlib.pyplot as plt
#############################################################################
#                             END OF YOUR CODE                              #
#############################################################################


def get_parser():
    parser = argparse.ArgumentParser(description="Points Selection")
    parser.add_argument("image1", type=str, help="path to image 1")
    parser.add_argument("image2", type=str, help="path to image 2")
    return parser


def pick_points(img1, img2):
    """
    Functionality to get manually identified corresponding points from two views.

    Inputs:
    - img1: The first image to select points from
    - img2: The second image to select points from

    Output:
    - coords1: An ndarray of shape (N, 2) with points from image 1
    - coords2: An ndarray of shape (N, 2) with points from image 2
    """
    ############################################################################
    # TODO: Implement pick_points
    ##########################################
    coords1 = []
    coords2 = []

    def onClick(event):
        x, y = event.xdata, event.ydata
        if event.inaxes == ax1:
            print(x, y)
            ax1.scatter(x, y, color='b')
            fig.canvas.draw()
            coords1.append((x, y))
        if event.inaxes == ax2:
            print(x, y)
            ax2.scatter(x, y, color='r')
            fig.canvas.draw()
            coords2.append((x, y))

    fig, (ax1, ax2) = plt.subplots(nrows=1, ncols=2,
                                   figsize=(15, 5), sharex=False, sharey=False)
    ax1.imshow(img1)
    ax1.axis('off')
    ax1.set_title('Image 1', fontsize=10)

    cid1 = fig.canvas.mpl_connect('button_press_event', onClick)

    ax2.imshow(img2)
    ax2.axis('off')
    ax2.set_title('Image 2', fontsize=10)

    plt.show()
    fig.canvas.mpl_disconnect(cid1)

    return np.array(coords1), np.array(coords2)
    ############################################################################
    #                             END OF YOUR CODE
    ############################################################################


if __name__ == "__main__":
    args = get_parser().parse_args()

    renders = sorted(glob.glob('Bar/' + "*.jpg"))
    img1 = imread(renders[34])
    img2 = imread(renders[34 + int(len(renders)/2)])

    coords1, coords2 = pick_points(img1, img2)

    assert len(coords1) == len(
        coords2), "The number of coordinates does not match"

    filename1 = os.path.splitext(args.image1)[0] + ".npy"
    filename2 = os.path.splitext(args.image2)[0] + ".npy"

    # assert not os.path.exists(filename1), f"Output file {filename1} already exists"
    # assert not os.path.exists(filename2), f"Output file {filename2} already exists"

    np.save(filename1, coords1)
    np.save(filename2, coords2)

    plt.show()
