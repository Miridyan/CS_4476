import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Cursor, Button


def pick_points(img, text):
    """
    Functionality to get 4 points.

    Inputs:
    - img: The image to select points from

    Output:
    - coords1: An ndarray of shape (N, 2) with points from image
    """
    ############################################################################
    # TODO: Implement pick_points
    ############################################################################
    coords = []
    fig, ax = plt.subplots(nrows=1, ncols=1)
    props = dict(boxstyle='round', facecolor='red', alpha=0.5)
    ax.set_title(text)
    ax.imshow(img)

    cursor = Cursor(ax, useblit=True, color='r', linewidth=1.0)

    def onclick(event):
        x, y = event.xdata, event.ydata
        ax.scatter(x, y, color='r')
        coords.append((x, y))
        fig.canvas.draw()
        if len(coords == 4):
            return np.asarray(coords)
    fig.canvas.mpl_connect('button_press_event', onclick)
    plt.show()
    return np.asarray(coords)
    ############################################################################
    #                             END OF YOUR CODE
    ############################################################################
