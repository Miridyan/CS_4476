#!/bin/env python

import glob
import math
import numpy as np
import matplotlib.pyplot as plt

from utils import *
from imageio import imread, imsave

PLOT_ITERATION = False
CUSTOM_IMAGE_PATH = 'h2_og.png'
CUSTOM_IMAGE_TEMPLATE_PATH = 'h2_template.png'
USING_CUSTOM_IMAGE = True

h = np.array([-8.57408126e-02, -5.87886223e-03, -8.88558391e+00,  8.24877640e-02, -9.13082003e-02, -1.48004093e+00, -8.99564278e-05, -9.16788817e-05])

img0 = rgb2gray(imread(CUSTOM_IMAGE_TEMPLATE_PATH))
img1 = rgb2gray(imread(CUSTOM_IMAGE_PATH))

new_img = apply_homography(img1, params_to_mat(h)) 

# print(test.shape)
# print(img0.shape)

# print(np.max(test[:, :, 0]))
# print(np.min(test[:, :, 0]))

# print(np.max(test[:, :, 1]))
# print(np.min(test[:, :, 1]))

# new_img = np.zeros_like(img0)
# height, width = new_img.shape
# cols, rows = np.indices((height, width))
# # xs = np.where((test < np.array([height, width])) & (test >= np.array([0, 0])))
# # ys, xs, gs = xs
# # print(np.min(test[ys, xs], axis=0))
# print(np.max(test, axis=0))
# print(test.shape)
# mask2 = mask[:, :, 0]
# print(mask2.shape)
# print(cols[mask2].shape)
# # print(cols[mask[:, 0]])

# print(test[mask].shape)
# (a,) = test[mask].shape
# b = test[mask].reshape((a//2, 2))

# print(np.max(b[:, 1]))
# print(np.max(cols[mask2]))
# print(np.max(rows[mask2]))

# print(new_img.shape)

# new_img[cols[mask2], rows[mask2]] = img1[b[:, 0], b[:, 1]]




# d = test[ys, xs]
# print(d.shape)

# ix, xs1 = np.where((d < np.array([height, width])) & (d >= np.array([0, 0])))


# print(np.max(d[ix], axis=0))

# dur = img0[(np.array([0, 0]) <= test[xs]) & (test[xs] < np.array([height, width]))]

# print(xs)

# for x in range(width):
#     for y in range(height):
#         if test[y, x, 0] < height and test[y, x, 0] >= 0 and test[y, x, 1] < width and test[y, x, 1] >= 0:
#             new_img[y, x] = img1[test[y, x, 0], test[y, x, 1]]

fig, (ax0, ax1, ax2) = plt.subplots(1, 3)

ax0.imshow(img0)
ax1.imshow(img1)
ax2.imshow(new_img)

plt.show()