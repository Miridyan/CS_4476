<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Computer Vision Class Project | ECE, Virginia Tech | Fall 2015: ECE 5554/4984</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet" />
    <style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
      .vis {
        color: #3366cc;
      }
      .data {
        color: #ff9900;
      }
    </style>

    <link href="css/bootstrap-responsive.min.css" rel="stylesheet" />

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <style>
      * {
        box-sizing: border-box;
      }

      .column2 {
        float: left;
        width: 33.33%;
        padding: 0px;
      }

      .column {
        float: left;
        width: 25%;
        padding: 0px;
      }

      .column3 {
        float: left;
        width: 50%;
        padding: 0px;
      }

      /* Clearfix (clear floats) */
      .row::after {
        content: "";
        clear: both;
        display: table;
      }
    </style>
  </head>

  <body>
    <div class="container">
      <div class="page-header">
        <!-- Title and Name -->
        <h1>3D Mesh Reformation From Deforming Non Rigid Surfaces</h1>
        <span style="font-size: 20px; line-height: 1.5em"
          ><strong>Youssef Asaad, Michael Ditto, Octavien Han</strong></span
        ><br />
        <span style="font-size: 18px; line-height: 1.5em"
          >Fall 2020 CS 4476 Computer Vision: Class Project</span
        ><br />
        <span style="font-size: 18px; line-height: 1.5em">Georgia Tech</span>
        <hr />

        <!-- Abstract -->
        <h3>Abstract</h3>
        In an effort to replicate the results from the Efficient Tracking of Regular Patterns on
        Non-rigid Geometry paper
        <a href="./Efficient_Tracking_of_Regular_Patterns_on_Non-rigid_Geometry.pdf" target="_blank"
          >[1]</a
        >
        we came across the fast template matching derived from
        <a href="./Equivalence and Efficiency_of_Image_Alignment_Algorithms.pdf" target="_blank"
          >[2]</a
        >. As we saw the variety of applications from this algorithm we decided to implement an
        efficient version of the algorithm from scratch. In the remainder of this report we will
        show how our implementation performs under different conditions and highlight the strengths
        and weaknesses. We will also discuss the pros and cons of the corner detection algorithm
        proposed by Igor Guskov in
        <a href="./Efficient_Tracking_of_Regular_Patterns_on_Non-rigid_Geometry.pdf" target="_blank"
          >[1]</a
        >
        and how it compares with existing algorithms. <br /><br />
        <div style="text-align: center">
          <h5 style="text-align: center">Teaser Figure</h5>
          <video
            loop
            autoplay
            height="500"
            name="Bar Video Left Camera"
            src="Renders/teaser_figure.mov"
            style="width: 100%"
          ></video>
        </div>
        <!-- Introduction -->
        <h3>Introduction</h3>
        Our main motivation behind solving this problem was to use this algorithm to perfom fast and
        robust corner detection with the end goal of efficiently tracking regular patterns on
        non-rigid geometries for 3-D reconstruction. The uses of the inverse compositional warp
        algorithm, however, range well beyond this application, and commonly include optical flow,
        object tracking, image stitching, and 3-D reconstruction. Our approach focuses on a
        deformable surface marked with black and white quads which can be deformed and observed from
        two camera angles in order to recover depth information. The quad markings enable better
        isolation of the quads from their surroundings in order to exclude the background of the
        image from the foreground element. There are many approaches to problems like this like
        stereophotogrammetry which can recover depth information from a scene given two camera
        angles. The primary differentiator of our approach is that it does not attempt to recover
        the depth information from the entire scene, and only the depth information of the
        quad-marked surface. Also, our approach tracks how the surface changes over time, and can be
        used to isolate the quad-marked surface in a continuous video stream to calculate the depth
        information every frame, allowing for detailed tracking of the deformation of a surface.
        Combining optical flow with the tracking of a quad-marked surface and depth calculation
        could have useful applications for computer animation or even the conversion of real-world
        objects into 3D models.
        <br /><br />
        <!-- Approach -->
        <h3>Approach</h3>
        We implemented the inverse compositional algorithm as described in the two existing research
        papers
        <a
          href="./The Inverse Compositional Algorithm for Parametric Registration.pdf"
          target="_blank"
          >[3]</a
        >
        <a href="./Lucas-Kanade_20_Years_On-_A_Unifying_Framework.pdf" target="_blank">[4]</a>. The
        idea of the inverse compositional algorithm is to reverse the role of an image and template
        when performing image alignment and works as follows: First a template is created to to
        define the end result which is to be found within an input image. A region of interest is
        then selected in the image to focus the search results. This region is used to approximate
        the starting homogrophy in which the iterative process begins. The template is then used to
        calculate the gradients in the x and y directions which are used in combination with the
        starting homography parameters to calculate the Jacobian matrix of the template, which
        contains the partial derivatives with respect to each paramter. A steepest descent matrix is
        then calculated based off of the Jacobian and the gradients. This matrix is used in every
        iteration for determining the parameter updates for the homography using gradient descent.
        Since the Jacobian, Hessian, and steepest descent matrices are all dependent on the
        template, and since the template does not change (due to role of the template and image
        being swapped), these values can be precomputed once, which is where the speed of this
        algorithm comes in. The key behind the efficiency of the inverse compositional algorithm is
        that the role of the image and the template are reversed, and as such the Hessian
        calculation can be done without relying on the parameters of the homography. <br /><br />Our
        end goal is based off of two existing research papers<sup
          ><a
            href="./Efficient_Tracking_of_Regular_Patterns_on_Non-rigid_Geometry.pdf"
            target="_blank"
            >[1]</a
          ></sup
        ><sup
          ><a href="./Real-time_Tracking_of_Quad-Marked_Surfaces.pdf" target="_blank">[5]</a></sup
        >
        that present a real time tracking solution for regular patterns on non-rigid geometry. These
        solutions work by tracking the motion of a checkerboard pattern on a flexible surface
        through a predictive algorithm based on previous frames with a combination of alignment
        techniques. The system first tracks dark quads indivudally with an associated homography
        such that each quad matches the image of a black unit square, and creates the following
        associated planar transform for each quadrant:
        <img src="planar_transform.png" alt="Planar transform" width="300" height="300" />
        The quads are tracked by using the two previous homographies (if available) to predict the
        next, under the assumption of a constant "homographic velocity". Starting from an initial
        estimate, the image alignment algorithm of
        <a href="./Equivalence and Efficiency_of_Image_Alignment_Algorithms.pdf" target="_blank"
          >[1]</a
        >
        is used. We have implemented the described inverse compositional image alignment algorithm
        developed from scratch in python with the aid of some libraries for efficiency.
        Specifically, this algorithm works by minimizing the error function
        <img src="mimize.png" alt="Error Function" width="200" height="200" /> over the desired
        template to detect quads. For our solution, we create a cross-like template:
        <img
          style="border: 1px solid #000000"
          src="template.jpg"
          alt="Template"
          width="100"
          height="100"
        />
        and use the inverse compositional image alignment algorithm to incrementely create a
        homography that warps the image to template. The speed of this aglorithm comes from warping
        the image to match the template as this allows the hessian and jacobian matrices to be
        precomputed once instead of computed with each iteration. Additionally, a pixel threshold of
        common point difference is used to determine if two quads are consistent. This homographic
        based temporal prediction accounts for both translation and rotation. An example of the
        desired solution can be seen in the image below: <br />
        <div style="text-align: center">
          <img src="glove.png" alt="Glove example" width="300" height="600" />
        </div>
        <br />
        These approaches can be expanded by using two cameras to create a 3D mesh representation
        from two stereo frames. In order to localize the cameras in 3D space, image warping and
        homography techniques similar to the ones taught in class can be used. In this case however,
        the known grid pattern can be used to localize the two cameras as seen in the images<sup
          ><a href="https://docs.opencv.org/master/d9/dab/tutorial_homography.html" target="blank"
            >[4]</a
          ></sup
        >
        below:
        <div style="text-align: center">
          <a href="https://docs.opencv.org/master/d9/dab/tutorial_homography.html" target="blank">
            <img src="homography_transformation_example2.jpg" alt="Homography example"
          /></a>
        </div>
        <br /><br />

        <br /><br />
        <!-- Results -->
        <h3>Experiments and results</h3>
        Below we can see the results (both success and failure cases) from our implementation. The
        success cases demonstrate how the algorithm is able to incrementally warp the image to match
        the template. The difference image shows the difference between the template and output at
        each iteration and is used to update the homography through gradient descent by calculating
        where the largest regions of error are. Our implementation does not currently use any
        bilinear interpolation which accounts for the little noise in the final difference images.
        Because of this, the sum of square difference error will never fall below a certain
        threshold. However, after enough iterations the homography still converges to a transform
        we'd expect and visually the results appear as though they match. As the error of the
        difference image is minimized, the algorithm detects that it is getting closer to its
        desired match and therefore slows down the changes in the homographies.
        <div style="text-align: center" class="row">
          <div class="column3">
            <h5 style="text-align: center">Success Case 1</h5>
            <video
              loop
              autoplay
              height="400"
              name="Bar Video Left Camera"
              src="Renders/h2_results_full.mov"
              style="width: 100%"
            ></video>
          </div>
          <div class="column3">
            <h5 style="text-align: center">Success Case 2</h5>
            <video
              loop
              autoplay
              height="400"
              name="Bar Video Left Camera"
              src="Renders/pepper_results_full.mov"
              style="width: 100%"
            ></video>
          </div>
        </div>

        Our failure cases demonstrates one of the key weaknesses in the algorithm. Since these
        approaches are typically used for tracking and optical flow, they assume that there are
        minimal changes between each frame. Therefore if the inital region of interest selected is
        too large, the algorithm fails to align the template as the gradient decent model is only
        calculated on the pixels within the template. If too many unknown pixels are added it
        introduces unknown results in the parameter updates and therefore the homgoraphy begins to
        be pushed in the wrong direction, further worsening the accuracy. This can clearly be seen
        in the following results:

        <br /><br />

        <div style="text-align: center" class="row">
          <div class="column2">
            <h5 style="text-align: center">Good Region (Success)</h5>
            <video
              loop
              autoplay
              height="400"
              name="Bar Video Left Camera"
              src="Renders/teaser_figure.mov"
              style="width: 100%"
            ></video>
          </div>
          <div class="column2">
            <h5 style="text-align: center">Okay Region (Failure)</h5>
            <video
              loop
              autoplay
              height="400"
              name="Bar Video Left Camera"
              src="Renders/okay_region.mov"
              style="width: 100%"
            ></video>
          </div>
          <div class="column2">
            <h5 style="text-align: center">Bad Region (Failure)</h5>
            <video
              loop
              autoplay
              height="400"
              name="Bar Video Left Camera"
              src="Renders/failure_case.mov"
              style="width: 100%"
            ></video>
          </div>
        </div>

        <br /><br />
        The proposed corner detection algorithm by Igor Guskov works within this constraint as it
        begins with the user selecting a region of interest for the initial homography. This region
        must be a good approxomation of the template in order to achieve a proper starting point.
        After the inverse compositional algorithm is run for the starting checker, the neighboring
        checkers are predicted by using the resultant homography from the initial checker. The
        expanded homography will be a good starting point for the immediate neighbors as long as the
        warped surface is within reason, however to account for extreme warps a smaller checker
        pattern can be used as each checker will more closely resemble the warp of its neighbors.
        The corners of the neighbors are then calculated using the inverse compositional warp and
        the process is repeated to grow the active region across the checkerboard.
        <br /><br />
        <div style="text-align: center">
          <img style="height: 400px" alt="" src="Sum_of_Squared_Difference_error.png" />
        </div>
        <br /><br />
        In the figure describing the Sum of Squared Difference Error over Iterations it is clear
        that the algorithm aims to reduce the difference between each iteration. For the "Good
        Region" the algorithm's gradient descent works very well as the specified region was close
        to the template and thus minimal unknown pixel values were present. In the two failure cases
        we can see that the results are very sporadic and the difference never converges to a
        minimal value. This is due to the fact that the algorithm is very sensitive to the template
        choice. We found that the best results occur when using a section of the input image as the
        template as opposed to a precomputed one as different resolution images cause noise in the
        calculated gradients that affect the steepest update parameter updates.

        <br /><br />

        We have built a simulated environment within the 3D modeling software Blender to give us our
        initial experimental environment. We have our checkered pattern on a cloth that is being
        deformed by multiple objects or forces as well as a ball that is moving within a scene Each
        of these unique circumstances is captured from two different camera angles. These videos can
        be used as inputs with our implementation of the inverse compositional alignment in the
        future.

        <br /><br />
        <div class="row">
          <div class="column3">
            <h5 style="text-align: center">Bar Camera 1</h5>
            <video
              loop
              autoplay
              width="320"
              height="240"
              name="Bar Video Left Camera"
              src="Renders/Bar/Camera_1.mp4"
              style="width: 100%"
            ></video>
          </div>
          <div class="column3">
            <h5 style="text-align: center">Open CV Detection Camera 1</h5>
            <video
              loop
              autoplay
              width="320"
              height="240"
              name="Sphere Video Left Camera"
              src="Renders/CV_Detections/Open_Cv_Bar_Camera_1.mp4"
              style="width: 100%"
            ></video>
          </div>
        </div>
        <div class="row">
          <div class="column3">
            <h5 style="text-align: center">Stretch Camera 1</h5>
            <video
              loop
              autoplay
              width="320"
              height="240"
              name="Bar Video Left Camera"
              src="Renders/Stretch/Camera_1.mov"
              style="width: 100%"
            ></video>
          </div>

          <div class="column3">
            <h5 style="text-align: center">Open CV Detection Camera 1</h5>
            <video
              loop
              autoplay
              width="320"
              height="240"
              name="Sphere Video Left Camera"
              src="Renders/CV_Detections/Open_Cv_Stretch_flat_Camera_1.mp4"
              style="width: 100%"
            ></video>
          </div>
        </div>

        <br /><br />
        We are working to build this project with the combined resources of multiple papers.<sup
          ><a href="./bakerequivalence.pdf" target="_blank">[1]</a></sup
        ><sup
          ><a
            href="./Efficient_Tracking_of_Regular_Patterns_on_Non-rigid_Geometry.pdf"
            target="_blank"
            >[2]</a
          ></sup
        >
        <sup
          ><a href="./Real-time_Tracking_of_Quad-Marked_Surfaces.pdf" target="_blank">[3]</a></sup
        >

        All of our current work has been in implementing the Inverse Compositional Algorithm
        <sup><a href="./Equivalence and Efficiency_of_Image_Alignment_Algorithms.pdf">[4]</a></sup>
        <sup><a href="./Lucas-Kanade_20_Years_On-_A_Unifying_Framework.pdf">[5]</a></sup>
        which has proven to be key in allowing the algorithm to work at real time. We have found a
        few reference implementations to compare results with and have also been testing alternative
        methods of corner detection. We have looked at OpenCV's findchessboardcorners function which
        is primarily used for camera calibration. This algorithm expects a flat plane without any
        distortion. Therefore it is clear that for results with planar deformation, such as the flat
        stretch, the algorithm detects every corner. However, in situations where 3-D warping
        occurs, such as the cloth being disturbed by a bar, the algorithm fails to detect the
        overall chessboard and thus does not recognize any of the corners where there is partial
        occlusion. The algorithm we are currently implementing focuses on "growing" a grid from the
        starting checker and thus will identify any contiguous set of checkers. This has multiple
        advantages over the OpenCV function as the entire checkerboard boundary does not need to be
        present. The inverse compositional algorthim is also generalized to different types of warps
        including homographies and thus will be better suited for detecting 3D warps in perspective.
        The models were created with the assumption that little stretching would occur to the
        individual checkers by creating a dense grid to accururatly represent the warp with
        homographies. This is one advantage the OpenCV algorithm had as it was not restricted to the
        basis that the checkerboards straight lines must remain straight.
        <br /><br />
        On the notion of occlusion, we hypothesise that using the inverse compositional algorithm
        along with a growing function that relies on spatial and temporal cohesion will perform well
        on specific sections of a warped pattern but will not be able to identify multiple flat
        regions if they are not contiguous. Thus one experiment in the future would be to begin with
        multiple starting squares and see how this will effect the accuracy of the algorithm.
        Another idea is to automatically select the new points based on a Harris corner detector to
        find checker corners that are not contiguous and use those as new starting points. Then the
        checkerboard pattern can be composed of the union of all possible checkerboards potentially
        overcoming the contiguous restraint.
        <br /><br />
        <h4>References</h4>

        <a href="./Efficient_Tracking_of_Regular_Patterns_on_Non-rigid_Geometry.pdf" target="_blank"
          >[1]</a
        >
        I. Guskov, "Efficient tracking of regular patterns on non-rigid geometry,"
        <i>Object recognition supported by user interaction for service robots</i>, Quebec City,
        Quebec, Canada, 2002, pp. 1057-1060 vol.2, doi: 10.1109/ICPR.2002.1048487. <br /><br />
        <a href="./Equivalence and Efficiency_of_Image_Alignment_Algorithms.pdf" target="_blank"
          >[2]</a
        >
        S. Baker and I. Matthews, "Equivalence and efficiency of image alignment algorithms,"
        <i
          >Proceedings of the 2001 IEEE Computer Society Conference on Computer Vision and Pattern
          Recognition. CVPR 2001</i
        >, Kauai, HI, USA, 2001, pp. I-I, doi: 10.1109/CVPR.2001.990652. <br /><br />
        <a
          href="./The Inverse Compositional Algorithm for Parametric Registration.pdf"
          target="_blank"
          >[3]</a
        >Javier Sánchez,<i>The Inverse Compositional Algorithm for Parametric Registration</i>,
        Image Processing On Line, 6 (2016), pp. 212–232. https://doi.org/10.5201/ipol.2016.153
        <br /><br />
        <a href="./Lucas-Kanade_20_Years_On-_A_Unifying_Framework.pdf" target="_blank">[4]</a>
        S. Baker and I. Matthews, "Lucas-Kanade 20 Years On: A Unifying Framework,"
        <i>International Journal of Computer Vision Vol. 56 No. 3</i>, Pittsburg, PA, USA, 2004, pp.
        221-255<br /><br />
        <a href="./Real-time_Tracking_of_Quad-Marked_Surfaces.pdf" target="_blank">[5]</a>
        I. Guskov and B. Bryant, "Real-time tracking of quad-marked surfaces,"
        <i>Workshop on Motion and Video Computing, 2002. Proceedings.</i>, Orlando, FL, USA, 2002,
        pp. 253-256, doi: 10.1109/MOTION.2002.1182245. <br /><br />

        <a href="https://docs.opencv.org/master/d9/dab/tutorial_homography.html" target="_blank"
          >[6]</a
        >
        https://docs.opencv.org/master/d9/dab/tutorial_homography.html<br /><br />

        <!-- Main Results Figure -->
        <div style="text-align: center">
          <img style="height: 300px" alt="" src="results.png" />
        </div>
        <br /><br />

        <!-- Main Results Figure -->
        <div style="text-align: center">
          <img style="height: 300px" alt="" src="qual_results.png" />
        </div>
        <br /><br />

        <hr />
        <footer>
          <p>© Youssef Asaad, Michael Ditto, Octavien Han</p>
        </footer>
      </div>
    </div>

    <br /><br />
  </body>
</html>
